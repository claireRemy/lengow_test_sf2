Lengow Test Service
===================
---------------------------------

This service fetches XML from a remote web service and displays it using the APYDataGridBundle.

Install
-------------

- clone the git repository using the command :

		git clone  https://claireRemy@bitbucket.org/claireRemy/lengow_test_sf2.git

- using composer, install the project dependencies :
	
		php composer.phar install

- install assets :

        php app/console assets:install

- create the database using the symfony command :

		php app/console doctrine:database:create

- update the schema :

		php app/console doctrine:schema:update --force

- to test the bundle you will need to download phpunit.phar 

Usage 
-----------

- launch orders import 

		/import

- see all orders using the APYDataGridBundle

		/

- add an order using a Symfony form

		/submit

- list all orders using JSON format
		
		/api
		/api/json

- list all orders using YAML format

		/api/yml

- list specific order using JSON format

		/api/<orderID>

- launch phpunit tests : 

		php phpunit.phar -c app src/Lengow/TestBundle/Tests/

