<?php

namespace Lengow\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TestOrder
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TestOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $marketplace;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $idFlux;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $orderMrId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $orderRefId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return TestOrder
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return TestOrder
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return TestOrder
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderMrID
     *
     * @param string $orderMrId
     * @return TestOrder
     */
    public function setOrderMrId($orderMrId)
    {
        $this->orderMrId = $orderMrId;

        return $this;
    }

    /**
     * Get orderMrID
     *
     * @return string 
     */
    public function getOrderMrId()
    {
        return $this->orderMrId;
    }

    /**
     * Set orderRefId
     *
     * @param string $orderRefId
     * @return TestOrder
     */
    public function setOrderRefId($orderRefId)
    {
        $this->orderRefId = $orderRefId;

        return $this;
    }

    /**
     * Get orderRefId
     *
     * @return string 
     */
    public function getOrderRefId()
    {
        return $this->orderRefId;
    }
}
