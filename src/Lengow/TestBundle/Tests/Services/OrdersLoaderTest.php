<?php
/**
 * Created by PhpStorm.
 * User: Anha
 * Date: 11/11/15
 * Time: 22:09
 */

namespace Lengow\TestBundle\Tests\Services;


use Symfony\Component\DomCrawler\Crawler;

require_once __DIR__ . '/../../../../../app/AppKernel.php';

class OrdersLoaderTest extends \PHPUnit_Framework_TestCase {

    private $kernel;

    /* @var \Symfony\Component\DependencyInjection\Container */
    private $container;

    /* @var \Doctrine\ORM\EntityManager */
    private $em;

    /* @var \Lengow\TestBundle\Services\OrderLoader */
    private $orderLoader;

    /* @var string */
    private $testOrder;

    /**
     * Set up the test environment to have access to the OrdersLoader Service
     */
    public function setUp()
    {
        //Set up new test environment
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        //Get container and entity manager
        $this->container = $this->kernel->getContainer();
        $this->em        = $this->container->get('doctrine')->getManager();

        //Get orderLoader to test service
        $this->orderLoader = $this->container->get('lengow_test');

        //Set up test string
        $this->testOrder =  '<?xml version="1.0" encoding="ISO-8859-1"?>'
                            .'<statistics>'
                                .'<orders>'
                                    .'<order>'
                                        .'<order_id>LENGOWTEST001</order_id>'
                                        .'<order_mrid>LENGOWTEST001</order_mrid>'
                                        .'<order_refid>LENGOWTEST001</order_refid>'
                                        .'<idFlux>12345</idFlux>'
                                        .'<marketplace>LENGOWTEST001</marketplace>'
                                    .'</order>'
                               .'</orders>'
                            .'</statistics>';
        }

    /**
     * Checks that given a correctly formed XML, an order is persisted
     */
    public function testSaveSingleOrderPersistsOrder()
    {
        $order = new Crawler($this->testOrder);
        $order = $order->filterXPath('//order');

        $orderRepository = $this->em->getRepository('LengowTestBundle:TestOrder');

        //Check there is nothing in the DB before processing the order
        $this->assertNull($orderRepository->findOneByOrderId('LENGOWTEST001'));

        //Check process succeed and flush DB
        $this->assertTrue($this->orderLoader->saveSingleOrder($order));
        $this->em->flush();

        //Check that after process there is only one order with ID LENGOWTEST001
        $this->assertCount(1, $orderRepository->findByOrderId('LENGOWTEST001'));

        //Remove test entities
        $dbOrders = $orderRepository->findByOrderId('LENGOWTEST001');
        foreach($dbOrders as $dbOrder) {
            $this->em->remove($dbOrder);
        }
        $this->em->flush();

        //Assert the test entities are removed
        $this->assertCount(0, $orderRepository->findByOrderId('LENGOWTEST001'));
    }

    /**
     * Checks that function fails if we cannot fetch the XML feed,
     * Also checks if the remote web service returns a well formed value
     */
    public function testFetchesXML()
    {
        $this->assertTrue($this->orderLoader->fetchOrdersXML('http://test.lengow.io/orders-test.xml'));

        $this->assertFalse($this->orderLoader->fetchOrdersXML('ThisStringIsMalformed'));
    }

}
 