<?php

namespace Lengow\TestBundle\Tests\Controller;

use Lengow\TestBundle\Entity\TestOrder;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * Checks that the navbar is present and the right links are displayed
     *
     * @dataProvider navbarPagesProvider
     */
    public function testNavbar($page)
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $page);

        //Check there is a single navbar
        $this->assertCount(1, $crawler->filter('div#navbar'));

        //check the links it contains
        $this->assertTrue($crawler->filter('html:contains("View orders")')->count() === 1 );
        $this->assertTrue($crawler->filter('html:contains("Import orders")')->count() === 1 );
        $this->assertTrue($crawler->filter('html:contains("Submit new order")')->count() === 1 );
        $this->assertTrue($crawler->filter('html:contains("API")')->count() === 1 );
    }

    /**
     * Returns pages which have a navbar
     *
     * @return array
     */
    public function navbarPagesProvider()
    {
        return array(
            array('/'),
            array('/submit')
        );
    }

    /**
     * Tests the index page
     */
    public function testIndex()
    {
        $client  = static::createClient();

        $crawler = $client->request('GET', '/');

        //Check search grid is present
        $this->assertCount(1, $crawler->filter('div.grid-search'));

        //Check the results grid is present
        $this->assertCount(1, $crawler->filter('div.grid'));

        //Check there are orders listed
        $this->assertGreaterThanOrEqual(0, $crawler->filter('tr.grid-row-cells')->count());
    }

    /**
     * Tests the form submission on the /submit page
     */
    public function testSubmitPage()
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/submit');

        //Select form and fill it
        $form = $crawler->selectButton('Submit')->form();
        $form['lengow_testbundle_testorder[marketplace]'] = 'TESTLENGOW001';
        $form['lengow_testbundle_testorder[idFlux]'] = '1234';
        $form['lengow_testbundle_testorder[orderId]'] = 'TESTLENGOW001';
        $form['lengow_testbundle_testorder[orderMrId]'] = 'TESTLENGOW001';
        $form['lengow_testbundle_testorder[orderRefId]'] = 'TESTLENGOW001';

        //Submit form
        $crawler = $client->submit($form);

        //Check that the process was a success
        $em = $client->getContainer()->get('doctrine')->getManager();
        $orderRepository = $em->getRepository('LengowTestBundle:TestOrder');
        $orders = $orderRepository->findByOrderId('TESTLENGOW001');
        $this->assertCount(1, $orders);

        //remove created entities
        foreach($orders as $order) {
            $em->remove($order);
        }
        $em->flush();
    }

    /**
     * Tests the API returns a JSON format by default
     *
     * @group API
     */
    public function testJsonFormatAPI()
    {
        $client = static::createClient();
        $client->request('GET', '/api');

        //Assert the response status code is 2xx
        $this->assertTrue($client->getResponse()->isSuccessful());

        //Assert that the Content Type header is JSON by default
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );

        //Check content is JSON
        $this->assertJson($client->getResponse()->getContent());
    }

    /**
     * Tests the API returns a YML format when the format yml is requested
     *
     * @group API
     */
    public function testYmlFormatAPI()
    {
        $client = static::createClient();

        //Get Entity Manager
        $em = $client->getContainer()->get('doctrine')->getManager();

        //Create test object and flush it to DB
        $order = new TestOrder();
        $order->setOrderRefId('TESTLENGOW001');
        $order->setOrderId('TESTLENGOW001');
        $order->setIdFlux(12345);
        $order->setOrderMrId('TESTLENGOW001');
        $order->setMarketplace('TESTLENGOW001');

        $em->persist($order);
        $em->flush();

        $client->request('GET', '/api/yml');

        //The yml format starts with '-'
        $this->assertStringStartsWith('-', $client->getResponse()->getContent());

        //Remove test objects
        $orderRepository = $em->getRepository('LengowTestBundle:TestOrder');
        $orders = $orderRepository->findByOrderId('TESTLENGOW001');
        foreach($orders as $order) {
            $em->remove($order);
        }
        $em->flush();

    }

    /**
     * Tests that calling the API with a correct orderID returns the corresponding JSON object
     *
     * @group API
     */
    public function testAPIReturnsJSONObject()
    {
        $client = static::createClient();

        //Get Entity Manager
        $em = $client->getContainer()->get('doctrine')->getManager();

        //Create test object and flush it to DB
        $order = new TestOrder();
        $order->setOrderRefId('TESTLENGOW001');
        $order->setOrderId('TESTLENGOW001');
        $order->setIdFlux(12345);
        $order->setOrderMrId('TESTLENGOW001');
        $order->setMarketplace('TESTLENGOW001');

        $em->persist($order);
        $em->flush();

        //Request test object
        $client->request('GET', '/api/TESTLENGOW001');

        //Test headers
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );

        //Test status code is 200
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );

        //Test object is a JSON object
        $this->assertJson($client->getResponse()->getContent());

        //Test is the requested object
        $this->assertContains('TESTLENGOW001', $client->getResponse()->getContent());

        $orderRepository = $em->getRepository('LengowTestBundle:TestOrder');
        $orders = $orderRepository->findByOrderId('TESTLENGOW001');
        foreach($orders as $order) {
            $em->remove($order);
        }
        $em->flush();

    }

    /**
     * Tests that requesting a non-existent orderID returns a 404 status code
     *
     * @group API
     */
    public function testAPIReturns404StatusCodeWhenObjectNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/ThisObjectDoesNotExist');

        //Assert status code is 404
        $this->assertEquals(
            404,
            $client->getResponse()->getStatusCode()
        );
    }
}
