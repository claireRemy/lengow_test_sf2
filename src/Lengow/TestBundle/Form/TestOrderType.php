<?php

namespace Lengow\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TestOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marketplace', 'text')
            ->add('idFlux', 'integer')
            ->add('orderId', 'text')
            ->add('orderMrId', 'text')
            ->add('orderRefId', 'text')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lengow\TestBundle\Entity\TestOrder'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lengow_testbundle_testorder';
    }
}
