<?php

namespace Lengow\TestBundle\Controller;

use APY\DataGridBundle\Grid\Source\Entity;
use Lengow\TestBundle\Entity\TestOrder;
use Lengow\TestBundle\Form\TestOrderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends Controller
{
    /**
     * Imports Orders from remote XML feed using the lengow_test service
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function importAction()
    {
        //Get OrdersLoader service
        $loader = $this->container->get('lengow_test');

        //Import orders
        $success = $loader->importOrders();

        if($success) {
            return $this->redirect($this->generateUrl('lengow_test_index'));
        } else {
            return new Response('An error occurred during order import', 500);
        }
    }

    /**
     * Displays Orders using the APY\DataGridBundle
     *
     * @return Response
     */
    public function indexAction()
    {
        //Initialize data source
        $source = new Entity('LengowTestBundle:TestOrder');

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid   = $this->container->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse('LengowTestBundle:Default:index.html.twig');
    }

    /**
     * Submits a new order using a form
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function submitAction(Request $request)
    {
        $order = new TestOrder();
        /* @var $form TestOrderType */
        $form = $this->get('form.factory')->create(new TestOrderType(), $order);

        if($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                try {
                    $dbOrder = $em->getRepository('LengowTestBundle:TestOrder')
                                  ->findOneBy(array('orderId' => $order->getOrderId()));

                    //Only store order if order doesn't exist in DB
                    if(null === $dbOrder) {
                        $em->persist($order);
                        $em->flush();
                        $request->getSession()->getFlashBag()->add('success', 'Order submitted successfully');

                        //Redirect to new form if success
                        return $this->redirect($this->generateUrl('lengow_test_submit'));
                    }

                    $request->getSession()->getFlashBag()->add('notice', 'This order already exists');
                } catch (\Exception $e) {
                    $request->getSession()->getFlashBag()->add('notice', 'Error with form submission');
                }
            }
        }

        return $this->render('LengowTestBundle:Default:submit.html.twig', array('form' => $form->createView()));
    }

    /**
     * Lists all orders from database in json or yml
     *
     * @param $_format request format
     * @return Response
     */
    public function listAllAction($_format)
    {
        $orders = $this->getDoctrine()->getRepository('LengowTestBundle:TestOrder')->findAll();
        $serializer = $this->container->get('jms_serializer');

        return new Response(
            $serializer->serialize($orders, $_format),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    /**
     * Returns the requested $order in json format
     *
     * @param \Lengow\TestBundle\Entity\TestOrder $order
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("order", class="LengowTestBundle:TestOrder", options={"repository_method" = "findOneByOrderId"})
     */
    public function getSingleOrderAction(TestOrder $order)
    {
        $serializer = $this->container->get('jms_serializer');

        return new Response(
            $serializer->serialize($order, 'json'),
            200,
            array('Content-Type' => 'application/json')
        );
    }
}
