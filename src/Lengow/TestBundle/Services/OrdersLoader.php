<?php

namespace Lengow\TestBundle\Services;

use Lengow\TestBundle\Entity\TestOrder;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class OrdersLoader
 * Loads the XML file from the remote $urlOrders URL
 *
 * @package Lengow\TestBundle\Services
 */
class OrdersLoader {

    /**
     * url pointing to the XML file we want to load
     * @var string
     */
    private $urlOrders;

    /**
     * the XML document as a Symfony Crawler
     * @var Crawler
     */
    private $xmlOrders;

    /**
     * The monolog logger
     * @var \Monolog\Logger
     */
    private $logger;

    /**
     * The Doctrine entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;


    /**
     * Instantiate OrdersLoader
     * @param $urlOrders
     * @param $logger
     * @param $em
     */
    public function __construct($urlOrders, $logger, $em)
    {
        $this->urlOrders   = $urlOrders;
        $this->logger      = $logger;
        $this->em          = $em;
    }

    /**
     * Fetches the XML file from the remote URL
     * Saves all order in DB
     *
     * @return bool success|failure
     */
    public function importOrders()
    {
        //Fetch XML
        $success = $this->fetchOrdersXML($this->getUrlOrders());

        //Save orders
        if($success) {
            $success = $this->saveOrders($this->getXmlOrders());
        }

        return $success;
    }

    /**
     * Fetches the XML file from $urlOrders url and stores it into $xmlOrders
     *
     * @param $urlOrders : the url from which to fetch the XML feed
     * @return string|null : xml as string if success, null if failure
     */
    public function fetchOrdersXML($urlOrders)
    {
        //Fetch remote XML file into string
        $this->logger->addInfo('Fetching remote XML feed at ' . $this->getUrlOrders());

        try {
            $xmlString = file_get_contents($urlOrders);
        } catch (\Exception $e) {
            $this->logger->addError('Fetching XML failed');
            return false;
        }

        //log XML string
        $this->logger->addInfo($xmlString);

        //Get Symfony Crawler from string and filter orders
        $xmlOrders = new Crawler($xmlString);
        $xmlOrders = $xmlOrders->filterXPath('//order');
        $this->setXmlOrders($xmlOrders);

        return true;
    }

    /**
     * Processes orders and saves them to DB
     *
     * @param \Symfony\Component\DomCrawler\Crawler $xmlOrders
     * @return bool success|failure
     */
    public function saveOrders(Crawler $xmlOrders)
    {
        //Process each order
        $xmlOrders->each(function ($order) {
            $this->saveSingleOrder($order);
        });

        //Flush changes to DB once
        try {
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->addError($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Takes the \Crawler $order and creates
     * the corresponding Doctrine entity in DB
     *
     * @param Crawler $order
     * @return bool true if success, false if an exception was raised
     */
    public function saveSingleOrder(Crawler $order)
    {
        //Get values from XML
        try {
            $orderId     = $order->filterXPath('//order_id')->text();
            $orderMrId   = $order->filterXPath('//order_mrid')->text();
            $orderRefId  = $order->filterXPath('//order_refid')->text();
            $idFlux      = $order->filterXPath('//idFlux')->text();
            $marketplace = $order->filterXPath('//marketplace')->text();
        } catch (\Exception $e) {
            $this->logger->addError($e->getMessage());
            return false;
        }

        //look for order in DB
        /* @var TestOrder|null */
        $dbOrder = $this->em->getRepository('LengowTestBundle:TestOrder')
                            ->findOneBy(array('orderId' => $orderId));

        //If Order doesn't exist in DB
        if(null === $dbOrder) {
            //create new order
            $dbOrder = new TestOrder();

            //set values
            $dbOrder->setIdFlux($idFlux);
            $dbOrder->setMarketplace($marketplace);
            $dbOrder->setOrderId($orderId);
            $dbOrder->setOrderMrID($orderMrId);
            $dbOrder->setOrderRefId($orderRefId);

            //persist to DB
            try {
                $this->em->persist($dbOrder);
            } catch (\Exception $e) {
                $this->logger->addError($e->getMessage());
                return false;
            }
        }

        return true;
    }

    /**
     * Get urlOrders
     * @return string
     */
    public function getUrlOrders()
    {
        return $this->urlOrders;
    }

    /**
     * Set xmlOrders
     * @param \Symfony\Component\DomCrawler\Crawler $xmlOrders
     */
    public function setXmlOrders(Crawler $xmlOrders)
    {
        $this->xmlOrders = $xmlOrders;
    }

    /**
     * Get xmlOrders
     * @return \Symfony\Component\DomCrawler\Crawler $xmlOrders
     */
    public function getXmlOrders()
    {
        return $this->xmlOrders;
    }
} 